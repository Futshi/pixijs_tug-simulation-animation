import { WINDOW, PLANET } from './constants.js';
import { Sprite } from './sprite.js'

export class Planet extends Sprite {
  constructor(sprite) {
    super(sprite); // parent constructor
    this.theta = Math.random() * 360;
    this.clockwise = Math.round(Math.random());
    this.speed = Math.min(0.015, Math.random() / 20);
    this.radius = 0;
    this.particle;
  }

  addParticleDynamics(particle) {
    this.particle = particle;
    particle.mass = Infinity;
    particle.G = radius;
  }

  setSpeed(speed) {
    this.speed = speed;
  }

  setRadius(radius) {
    this.radius = radius;
  }

  moveAroundParent(parent_position, parent_height) {  // true = out of area
    this.theta = (this.theta + this.speed) % 360;
    this.sprite.x = parent_position.x + this.radius;
    this.sprite.y = parent_position.y + this.radius;

    let sin = Math.sin(this.theta);
    let cos = Math.cos(this.theta);
    // translate point back to origin
    let origin_x = this.sprite.x - parent_position.x;
    let origin_y = this.sprite.y - parent_position.y;
    // rotate point
    let new_x = this.clockwise
      ? origin_x * cos + origin_y * sin    // clockwise
      : origin_x * cos - origin_y * sin;   // counter-clockwise
    let new_y = this.clockwise
      ? -origin_x * sin + origin_y * cos   // clockwise
      : origin_x * sin + origin_y * cos;   // counter-clockwise
    // translate point back
    this.sprite.x = new_x + parent_position.x;
    this.sprite.y = new_y + parent_position.y;
    // check bounds
    if(parent_position.y - parent_height - this.radius - this.sprite.height > WINDOW.height) {
      return true;
    }
    return false;
  }

  moveUntilOutOfArea() { // true = out of area
    this.sprite.y += this.speed;
    if(this.sprite.y - this.sprite.height > WINDOW.height) {
      return true;
    }
    return false;
  }
}
