/* global PIXI */

import { PLAYER, WINDOW, CONTROLS, TEXTURES } from './constants.js';
import { Sprite } from './sprite.js';
import { Controls } from './controls.js';

export class Player extends Sprite {
  constructor(sprite) {
    super(sprite); // parent constructor
    this.sprite.x = WINDOW.x + (WINDOW.width / 2) - (this.sprite.width / 2); // start position
    this.sprite.y = WINDOW.y + (WINDOW.height * 0.8); // start position
    this.shoot_cooldown = 0.0;
    this.health = PLAYER.health;
  }

  update(delta){
    // bullet loading time
      this.shoot_cooldown = this.shoot_cooldown < PLAYER.shoot_cooldown
        ? this.shoot_cooldown += delta
        : PLAYER.shoot_cooldown;
  }

  getHealth() { return this.health; }

  getPosition() {
    return {
      x: this.sprite.x,
      y: this.sprite.y
    };
  }

  receiveDamage(amount) {
    this.health -= amount;
    if(this.health <= 0) {
      // game over
      return true;
    }
    return false;
  }

  canShoot(){
    if(this.health > 0 && this.shoot_cooldown >= PLAYER.shoot_cooldown) {
      return true;
    }
  }

  shoot(){
    if(this.canShoot())
    {
      this.shoot_cooldown = 0.0;
      return true;
    }
    return false;
  }

  move(controls) {
    if(this.health > 0) {
      // move up
      if(controls.getKey(CONTROLS.move_up)) {
        this.sprite.y =  this.sprite.y <= 0
          ? 0
          : this.sprite.y - PLAYER.speed;
      }
      // move left
      if(controls.getKey(CONTROLS.move_left)) {
        this.sprite.x =  this.sprite.x <= 0
          ? 0
          : this.sprite.x - PLAYER.speed;
      }
      // move down
      if(controls.getKey(CONTROLS.move_down)) {
        let max_height = WINDOW.height - this.sprite.height;
        this.sprite.y =  this.sprite.y >= max_height
          ? max_height
          : this.sprite.y + PLAYER.speed;
      }
      // move right
      if(controls.getKey(CONTROLS.move_right)) {
        let max_width = WINDOW.width - this.sprite.width;
        this.sprite.x =  this.sprite.x >= max_width
          ? max_width
          : this.sprite.x + PLAYER.speed;
      }
    }
    return false;
  }
}
