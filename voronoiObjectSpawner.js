/* global PIXI */

import { TEXTURES, SPAWNER, WINDOW, VORONOI } from './constants.js';
import { VoronoiObject } from './voronoiObject.js';
import { Voronoi } from './voronoi.js';

export class VoronoiObjectSpawner {
  constructor(sprite_voronoi) {
    this.sprite = sprite_voronoi;
    this.cooldown = SPAWNER.voronoi_cooldown;
  }

  getRandomXSpawnPosition() {
    let min_x = WINDOW.x;
    let max_x = WINDOW.width - this.sprite.width;
    return Math.floor(Math.random() * (max_x - min_x + 1)) + min_x;
  }

  getNewVoronoiObject() {
    const voronoi_object = new VoronoiObject(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.voronoi].texture));
    voronoi_object.setPosition(this.getRandomXSpawnPosition(), -this.sprite.height);
    return voronoi_object;
  }

  spawnNewVoronoiObjectCheck(delta) {
    // enemy spawn interval
    this.cooldown += delta;
    if(this.cooldown >= SPAWNER.voronoi_cooldown) {
      this.cooldown = 0.0;
      return true;
    }
    return false;
  }
}