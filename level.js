import { WINDOW } from './constants.js';
import { Sprite } from './sprite.js';

export class Level extends Sprite {
  constructor(sprite) {
    super(sprite);  // parent constructor
    this.sprite.width = WINDOW.width;
    this.sprite.height = WINDOW.height;
  }
}