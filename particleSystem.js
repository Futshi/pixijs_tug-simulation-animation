import { ODES, PHYSICS } from './constants.js'
import { Particle } from './particle.js';

export class ParticleSystem {
  constructor() {
    this.particles = [];
  }

  addParticle() {
    let particle = new Particle();
    this.particles.push(particle);
    return particle;
  }

  removeParticle(particle) {
    for( let i = 0; i < this.particles.length; i++ ) {
      if(this.particles[i] === particle) {
          this.particles.splice(i, 1);
          return;
      }
    }
  }

  update(dt) {
    // find all gravity creating actors
    let gravity_actors = [];
    for( let i = 0; i < this.particles.length; i++ ) {
      let p = this.particles[i];
      if(p.g > 0) {
        gravity_actors.push(p.getGravity.bind(p));
      }
    }

    // Compute next simulation step
    let step = new Array(this.particles.length);
    for( let i = 0; i < this.particles.length; i++ ) {
      let p = this.particles[i];
      p.gs = gravity_actors;
      if(PHYSICS.ode === ODES.verlet) {
        step[i] = verlet(p.x, p.v, p.a, p.getForces.bind(p), dt);
      }
      else { // rk4
        step[i] = rk4(p.x, p.getForces.bind(p), dt);
      }
    }

    // Apply next simulation step
    for( let i = 0; i < this.particles.length; i++ ) {
      let p = this.particles[i];
      let next = step[i];
      if(PHYSICS.ode === ODES.verlet) {
        p.x = next[0];
        p.v = next[1];
        p.a = next[2];
      } else { // rk4
        p.x = next;
      }
    }
  }
}

function verlet(x, v, a, adt,  dt) {
  // var x2 = x + v * dt + 0.5 * a * dt * dt;
  var x2 = x;
  vec3.scaleAndAdd(x2, x2, v, dt);
  vec3.scaleAndAdd(x2, x2, a, 0.5 * dt * dt);

  var a2 = adt(x2);
  // var v2 = v + 0.5 * (a + a2) * dt;
  var v2 = v;
  var av2 = [];
  vec3.add(av2, a2, a); // (a + a2)
  vec3.scaleAndAdd(v2, v2, av2, 0.5 * dt);
  return [x2, v2, a2];
}

function rk4(x, dydt, dt) {
  var k1 = vec3.create();
  vec3.scale(k1, dydt(x), dt);

  var x2 = vec3.create();
  vec3.scaleAndAdd(x2, x, k1, 0.5);
  var k2 = vec3.create();
  vec3.scale(k2, dydt(x2), dt);

  var x3 = vec3.create();
  vec3.scaleAndAdd(x3, x, k2, 0.5);
  var k3 = vec3.create();
  vec3.scale(k3, dydt(x3), dt);

  var x4 = vec3.create();
  vec3.add(x4, x, k3);
  var k4 = vec3.create();
  vec3.scale(k4, dydt(x4), dt);

  var xn = vec3.create();
  vec3.scaleAndAdd(xn, x, k1, 1/6);
  vec3.scaleAndAdd(xn, xn, k2, 1/3);
  vec3.scaleAndAdd(xn, xn, k3, 1/3);
  vec3.scaleAndAdd(xn, xn, k4, 1/6);
  return xn;
}
