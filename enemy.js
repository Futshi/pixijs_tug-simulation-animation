import { WINDOW, ENEMY } from './constants.js';
import { Sprite } from './sprite.js'
import { Spline } from './catmullrom.js'

export class Enemy extends Sprite {
  constructor(sprite) {
    super(sprite); // parent constructor
    this.path = new Spline(this.getRandomPath(6, this.sprite.height, this.sprite.width));
    this.time = 0.0;
  }

  // generates a path with the first and last two control points outside the visible area.
  getRandomPath(len, sprite_width, sprite_height) {
    let random_length = len - 4;
    let path = [];
    if(random_length > 0){
      path = new Array(random_length).fill(0).map(function(n){
        return [Math.random() * (WINDOW.width - sprite_width), Math.random() * (WINDOW.height - sprite_height)];
      }).sort(function(a, b){return a[1] - b[1]});
    }

    let xmax = WINDOW.width-16;

    // point 0 way above window
    path.splice(0, 0, [WINDOW.width/2, -WINDOW.height/2]);
    // point 1 just above window
    path.splice(1, 0, [Math.random() * xmax, -(sprite_height+1)]);
    // point len-2 just below window
    path.splice(path.length, 0, [Math.random() * xmax, WINDOW.height+sprite_height]);
    // point len-1 way below window
    path.splice(path.length, 0, [WINDOW.width/2, 1.5*WINDOW.height]);

    return path;
  }

  setSpawnPosition(x, y) {
    this.sprite.x = x;
    this.sprite.y = y;
  }

  moveUntilOutOfArea() { // true = remove enemy flag
    this.sprite.y += ENEMY.speed;
    return this.sprite.y > WINDOW.height
  }

  moveSpline(t) { // true = remove enemy flag
    this.time += t * 0.01;
    let pos = this.path.constantSpeedSample(this.time);
    this.sprite.x = pos[0];
    this.sprite.y = pos[1];

    return this.sprite.y > WINDOW.height
  }
}
