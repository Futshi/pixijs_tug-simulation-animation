/* global PIXI */

import { TEXTURES, SPAWNER, WINDOW, PLANET} from './constants.js';
import { PlanetSystem } from './planetSystem.js';
import { Planet } from './planet.js';

export class PlanetSystemSpawner {
  constructor(sprite_planet_large, sprite_planet_medium, sprite_planet_small, sprite_planet_tiny) {
    this.sprite_planet_large = sprite_planet_large;
    this.sprite_planet_medium = sprite_planet_medium;
    this.sprite_planet_small = sprite_planet_small;
    this.sprite_planet_tiny = sprite_planet_tiny;
    this.cooldown = SPAWNER.planet_cooldown;
  }

  getRandomXSpawnPosition() {
    let min_x = WINDOW.x;
    let max_x = WINDOW.width - this.sprite_planet_large.width;
    return Math.floor(Math.random() * (max_x - min_x + 1)) + min_x;
  }

  getNewPlanetSystem() {
    const planet_system = new PlanetSystem();
    planet_system.setSpawnPosition(
      this.getRandomXSpawnPosition(), // make sure planet spawns above screen
      -(this.sprite_planet_large.height + PLANET.radius.medium + this.sprite_planet_medium.height + PLANET.radius.small + this.sprite_planet_small.height + PLANET.radius.tiny + this.sprite_planet_tiny.height));
    // get planets
    const planet_large = new Planet(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.planet_large].texture));
    const planet_medium = new Planet(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.planet_medium].texture));
    const planet_small = new Planet(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.planet_small].texture));
    const planet_tiny = new Planet(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.planet_tiny].texture));
    // set planets
    planet_system.setPlanets(planet_large, planet_medium, planet_small, planet_tiny);
    return planet_system;
  }

  spawnNewPlanetSystemCheck(delta) {
    // enemy spawn interval
    this.cooldown += delta;
    if(this.cooldown >= SPAWNER.planet_cooldown) {
      this.cooldown = 0.0;
      return true;
    }
    return false;
  }
}