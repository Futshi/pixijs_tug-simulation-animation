/* global PIXI */

import { WINDOW, TEXTURES, ENEMY, GAME, CONTROLS, HEALTH, DEVELOPMENT, VORONOI, EXPLOSION } from './constants.js'
import { Level } from './level.js';
import { Player } from './player.js';
import { Controls } from './controls.js';
import { EnemySpawner } from './enemySpawner.js';
import { PlanetSystemSpawner } from './planetSystemSpawner.js';
import { VoronoiObjectSpawner } from './voronoiObjectSpawner.js';
import { Bullet } from './bullet.js';
import { Health } from './health.js';
import { checkSpriteCollision } from './collision.js';
import { ParticleSystem } from './particleSystem.js';
import { ExplosionSpawner } from './explosionSpawner.js';

PIXI.Loader.shared
  .add([
    TEXTURES.level,
    TEXTURES.player,
    TEXTURES.bullet,
    TEXTURES.particle,
    TEXTURES.enemy,
    TEXTURES.health,
    TEXTURES.planet_large,
    TEXTURES.planet_medium,
    TEXTURES.planet_small,
    TEXTURES.planet_tiny,
    TEXTURES.voronoi
  ]).load(setup);

// the application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container
const app = new PIXI.Application({
  width: WINDOW.width,
  height: WINDOW.height,
  backgroundColor: 0x000000,
  resolution: window.devicePixelRatio || 1,
});
// the application will create a canvas element for you that you can then insert into the DOM
document.body.appendChild(app.view);
// a container represents a collection of display objects
const background_container = new PIXI.Container();
const game_container = new PIXI.Container();
app.stage.addChild(background_container, game_container);
// position of display objects on the x & y axis relative to local coordinates of parent
//game_container.x = app.screen.width / 2;
//game_container.y = app.screen.height / 2;

// ========================================================================
let controls, level, enemy_spawner, planet_system_spawner, voronoi_object_spawner, player, explosion_spawner, particle_system;
let pause_text, game_over_text;
let enemies = [], planet_systems = [], bullets = [], healths = [], voronoi_objects = [], voronoi_fragments = [];
let pause = false, pause_cooldown = 0.0;
let game_over = false;

function setup() {
  // spreatsheet "cut-outs"
  let frame_16x24 = new PIXI.Rectangle(0, 0, 16, 24);
  let frame_16x16 = new PIXI.Rectangle(0, 0, 16, 16);
  let frame_6x6 = new PIXI.Rectangle(5, 5, 6, 6);
  PIXI.Loader.shared.resources[TEXTURES.player].texture.frame = frame_16x24;
  PIXI.Loader.shared.resources[TEXTURES.enemy].texture.frame = frame_16x16;
  PIXI.Loader.shared.resources[TEXTURES.bullet].texture.frame = frame_16x16;
  PIXI.Loader.shared.resources[TEXTURES.health].texture.frame = frame_6x6;
  PIXI.Loader.shared.resources[TEXTURES.particle].texture.frame = frame_16x16;
  // sprites

  particle_system = new ParticleSystem();
  let window_gravity = particle_system.addParticle();
  window_gravity.setPosition([WINDOW.width/2, WINDOW.height * 2, 0]);
  window_gravity.setGravitation(1);
  window_gravity.setMass();

  level = new Level(new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.level].texture));
  background_container.addChild(level.getSprite());
  player = new Player(new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.player].texture));
  game_container.addChild(player.getSprite());
  enemy_spawner = new EnemySpawner(new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.enemy].texture));
  explosion_spawner = new ExplosionSpawner(particle_system, game_container);
  planet_system_spawner = new PlanetSystemSpawner(
    new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.planet_large].texture),
    new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.planet_medium].texture),
    new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.planet_small].texture),
    new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.planet_tiny].texture));
  voronoi_object_spawner = new VoronoiObjectSpawner(new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.voronoi].texture));
  for(let i = 0; i < player.getHealth(); i++) {
    let health = new Health(new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.health].texture));
    health.setRadius(HEALTH.padding + i * HEALTH.radius);
    game_container.addChild(health.getSprite());
    healths.push(health);
  }
  // texts
  pause_text = new PIXI.Text("PAUSE", { fill: "white", fontSize: 24, align: "center" });
  pause_text.anchor.set(0.5);
  pause_text.x = WINDOW.width / 2;
  pause_text.y = WINDOW.height / 2;
  game_over_text = new PIXI.Text("GAME OVER", { fill: "white", fontSize: 24, align: "center" });
  game_over_text.anchor.set(0.5);
  game_over_text.x = WINDOW.width / 2;
  game_over_text.y = WINDOW.height / 2;
  // controls
  controls = new Controls();
  // app settings
  app.renderer.render(app.stage);
  app.ticker.deltaMS = 166.66;
  app.ticker.add((delta) => update(delta)); // ticker for doing render updates
}

function gameOver() {
  game_container.addChild(game_over_text);
  game_over = true;
}

function update(delta) {
  // pause cooldown
  pause_cooldown = pause_cooldown < GAME.cooldown
    ? pause_cooldown += delta
    : GAME.cooldown;
  if(!game_over) {
    if(!pause && !game_over) {
      particle_system.update(delta);
      explosion_spawner.update(delta);
      player.update(delta);
      player.move(controls);
      if(controls.getKey(CONTROLS.shoot) && player.canShoot()) {
        player.shoot();
        // uncomment to test particle dynamics
        //explosion_spawner.spawnExplosion({x:WINDOW.width/2, y:WINDOW.height/2});
        // bullet spawn
        let bullet = new Bullet(new PIXI.Sprite(PIXI.Loader.shared.resources[TEXTURES.bullet].texture));
        bullet.setSpawnPosition(player.getPosition());
        bullets.push(bullet);
        game_container.addChild(bullet.getSprite());
      }
      // bullet update
      for(let i = 0; i < bullets.length; i++) {
        if(bullets[i].moveUntilOutOfArea()) {
          // bullet removal
          game_container.removeChild(bullets[i].getSprite());
          bullets.splice(i, 1);
        }
      }
      // mob spawn
      if(DEVELOPMENT.enemies_active && enemy_spawner.spawnNewEnemyCheck(delta)) {
        let enemy = enemy_spawner.getNewEnemy();
        enemies.push(enemy);
        game_container.addChild(enemy.getSprite());
      }
      // mob update
      for(let i = 0; i < enemies.length; i++) {
        //if(enemies[i].moveUntilOutOfArea()) {
        if(enemies[i].moveSpline(delta)) {
          // mob removal
          game_container.removeChild(enemies[i].getSprite());
          enemies.splice(i, 1);
        }
      }
      // planet spawn
      if(DEVELOPMENT.planets_active && planet_system_spawner.spawnNewPlanetSystemCheck(delta)) {
        let planet_system = planet_system_spawner.getNewPlanetSystem();
        planet_systems.push(planet_system);
        let planets = planet_system.getPlanets();
        for(let i = 0; i < planets.length; i++) {
          background_container.addChild(planets[i].getSprite());
        }
      }
      // planet update
      for(let i = 0; i < planet_systems.length; i++) {
        if(planet_systems[i].moveUntilOutOfArea()) {
          // planet removal
          let planets = planet_systems[i].getPlanets();
          for(let j = 0; j < planets.length; j++) {
            background_container.removeChild(planets[j].getSprite());
          }
          planet_systems.splice(i, 1);
        }
      }
      // voronoi object spawn
      if(DEVELOPMENT.voronoi_active && voronoi_object_spawner.spawnNewVoronoiObjectCheck(delta)) {
        let voronoi_object = voronoi_object_spawner.getNewVoronoiObject();
        // at the beginning voronoi is intact, so we can simply add the object
        voronoi_objects.push(voronoi_object);
        game_container.addChild(voronoi_object.getSprite());
      }
      // voronoi object update
      for(let i = 0; i < voronoi_objects.length; i++) {
        if(voronoi_objects[i].moveUntilOutOfArea()) {
          // object removal
          game_container.removeChild(voronoi_objects[i].getSprite());
          voronoi_objects.splice(i, 1);
        }
      }
      // (fractured) voronoi fragments update
      for(let i = 0; i < voronoi_fragments.length; i++) {
        if(voronoi_fragments[i].moveUntilOutOfArea()) {
          // fragment removal
          background_container.removeChild(voronoi_fragments[i].getSprite());
          background_container.removeChild(voronoi_fragments[i].getMask());
          voronoi_fragments.splice(i, 1);
        }
      }
      // health update
      for(let i = 0; i < healths.length; i++) {
        healths[i].move(player.getPosition());
      }
      // enemy collision detection
      for(let i = 0; i < enemies.length; i++) {
        // player check
        if(checkSpriteCollision(enemies[i].getSprite(), player.getSprite())) {
          game_container.removeChild(enemies[i].getSprite());
          enemies.splice(i, 1);
          if(!DEVELOPMENT.god_mode) { // added god mode :)
            // remove health objects according to damage
            for(let j = 0; j < ENEMY.collision_damage; j++) {
              if(player.getHealth() <= 0) { break; }
              game_container.removeChild(healths.pop().getSprite());
            }
            if(player.receiveDamage(ENEMY.collision_damage)) {
              gameOver();
            }
          }
          continue; // skip further checks
        }
        // bullet check
        for(let j = 0; j < bullets.length; j++) {
          if(checkSpriteCollision(enemies[i].getSprite(), bullets[j].getSprite())) {
            explosion_spawner.spawnExplosion(enemies[i].getPosition());
            game_container.removeChild(enemies[i].getSprite());
            game_container.removeChild(bullets[j].getSprite());
            bullets.splice(j, 1);
            break;
          }
        }
      }
      // collision detection: voronoi object
      for(let i = 0; i < voronoi_objects.length; i++) {
        // player check
        if(checkSpriteCollision(voronoi_objects[i].getSprite(), player.getSprite())) {
          // add new fragments to array and background
          let voronois = voronoi_objects[i].getFragments();
          for(let k = 0; k < voronois.length; k++) {
            voronoi_fragments.push(voronois[k]);
            background_container.addChild(voronois[k].getSprite());
            background_container.addChild(voronois[k].getMask());
          }
          game_container.removeChild(voronoi_objects[i].getSprite());
          voronoi_objects.splice(i, 1);
          if(!DEVELOPMENT.god_mode) { // added god mode :)
            // remove health objects according to damage
            for(let j = 0; j < VORONOI.collision_damage; j++) {
              if(player.getHealth() <= 0) { break; }
              game_container.removeChild(healths.pop().getSprite());
            }
            if(player.receiveDamage(VORONOI.collision_damage)) {
              gameOver();
            }
          }
          continue; // skip further checks
        }
        // bullet check
        for(let j = 0; j < bullets.length; j++) {
          if(checkSpriteCollision(voronoi_objects[i].getSprite(), bullets[j].getSprite())) {
            // add new fragments to array and background
            let voronois = voronoi_objects[i].getFragments();
            for(let k = 0; k < voronois.length; k++) {
              voronoi_fragments.push(voronois[k]);
              background_container.addChild(voronois[k].getSprite());
              background_container.addChild(voronois[k].getMask());
            }
            // voronoi object split into fragments (which are harmless and therefore but into background)
            game_container.removeChild(voronoi_objects[i].getSprite());
            game_container.removeChild(bullets[j].getSprite());
            voronoi_objects.splice(i, 1);
            bullets.splice(j, 1);
            break;
          }
        }
      }
    }
    // check for pause
    if(controls.getKey(CONTROLS.pause) && pause_cooldown >= GAME.cooldown) {
      pause_cooldown = 0.0;
      pause = !pause;
      if(pause) {
        game_container.addChild(pause_text);
      } else {
        game_container.removeChild(pause_text);
      }
    }
  }
}

// error handling
app.loader.onError.add((...args) => console.error(args));
