19.03.2020
- We on the same page now, yay

10.04.2020
- Basic functionality added (moving, shooting) + split into different files for better organization

15.04.2020
- More "organization" (spawner, controls, ...) added

28.04.2020
- Path interpolation