export const CONTROLS = {
  move_left: ["KeyA", "ArrowLeft"],
  move_right: ["KeyD", "ArrowRight"],
  move_up: ["KeyW", "ArrowUp"],
  move_down: ["KeyS", "ArrowDown"],
  shoot: ["Space"],
  pause: ["KeyP"],
}

export class Controls {
  constructor() {
    this.keys = {}; // object-key-value pair
    window.addEventListener("keydown", this.keyDown.bind(this));
    window.addEventListener("keyup", this.keyUp.bind(this));
  }

  keyDown(e) {
    this.keys[e.code] = true;
  }

  keyUp(e) {
    this.keys[e.code] = false;
  }

  getKeys() {
    return this.keys;
  }

  getKey(k) {
    for (const code in k) {
      if(this.keys[k[code]]){
        return true;
      }
    }
    return false;
  }
}
