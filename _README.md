# Worst-Space-Scenario
Space-Invaders like techdemo game

## Information
- Google Chrome does not natively enable WebGL? Not sure if I disabled it somehow, but it's better to run this project with Firefox
- Optionally, turn on WebGL in Chrome
  - https://ccm.net/faq/40585-how-to-enable-webgl-on-google-chrome

## PixiJS
- Examples
  - https://pixijs.io/examples/
- API Documentation
  - https://pixijs.download/release/docs/PIXI.Texture.html

## Tutorial
- https://www.youtube.com/watch?v=dDSKexgVCaU

## Assets
- Spaceship Shooter Environment (ansimuz)
  -  https://ansimuz.itch.io/spaceship-shooter-environment

## Getting started
- run `npx http-server -o`

