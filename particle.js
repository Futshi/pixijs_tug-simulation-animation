export class Particle {
  constructor() {
    this.x = vec3.create();
    this.v = vec3.create();
    this.a = vec3.create();
    this.drag = 0.1;
    this.mass = 1.0;

    // If the particle is a planet then enter its specific gravitational constant here
    this.g = 0.0;
    this.gs = [];
  }

  setDrag(d) {
    this.drag = d;
  }

  setPosition(pos) {
    this.x = pos;
  }

  setVelocity(vel) {
    this.v = vel;
  }

  setAcceleration(acc) {
    this.a = acc;
  }

  setMass(mass) {
    this.m = mass;
  }

  setGravitation(g) {
    this.g = g;
  }

  getFDrag() {
    let out = [];
    vec3.scale(out, this.v, -this.drag);
    return out;
  }

  getGravity(pos) {
    let out = vec3.create();
    // compute direction that gravity is pulling along
    vec3.sub(out, this.x, pos);
    vec3.normalize(out, out);
    // compute magnitude
    vec3.scale(out, out, this.mass * this.g);
    return out;
  }

  getForces(pos) {
    let f = vec3.create();
    vec3.add(f, f, this.getFDrag());
    // sum gravities
    for( let i = 0; i < this.gs.length; i++ ) {
      vec3.add(f, f, this.gs[i](pos));
    }
    vec3.scale(f, f, 1/this.mass);
    return f;
  }

  getPosition() {
    return this.x;
  }

}
